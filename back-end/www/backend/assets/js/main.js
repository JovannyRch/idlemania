import Camera from 'http://localhost:8181/backend/assets/js/Camera.js';
import Entity from 'http://localhost:8181/backend/assets/js/Entity.js';
import PlayerController from 'http://localhost:8181/backend/assets/js/traits/PlayerController.js';
import Timer from 'http://localhost:8181/backend/assets/js/Timer.js';
import {createLevelLoader} from 'http://localhost:8181/backend/assets/js/loaders/level.js';
import {loadFont} from 'http://localhost:8181/backend/assets/js/loaders/font.js';
import {loadEntities} from 'http://localhost:8181/backend/assets/js/entities.js';
import {setupKeyboard} from 'http://localhost:8181/backend/assets/js/input.js';
import {createCollisionLayer} from 'http://localhost:8181/backend/assets/js/layers/collision.js';
import {createDashboardLayer} from 'http://localhost:8181/backend/assets/js/layers/dashboard.js';

function createPlayerEnv(playerEntity) {
    const playerEnv = new Entity();
    const playerControl = new PlayerController();
    playerControl.checkpoint.set(64, 64);
    playerControl.setPlayer(playerEntity);
    playerEnv.addTrait(playerControl);
    return playerEnv;
}

async function main(canvas) {
    const context = canvas.getContext('2d');

    const [entityFactory, font] = await Promise.all([
        loadEntities(),
        loadFont(),
    ]);

    const loadLevel = await createLevelLoader(entityFactory);

    const level = await loadLevel('1-1');

    const camera = new Camera();

    const mario = entityFactory.mario();

    const playerEnv = createPlayerEnv(mario);
    level.entities.add(playerEnv);


    level.comp.layers.push(createCollisionLayer(level));
    level.comp.layers.push(createDashboardLayer(font, playerEnv));

    const input = setupKeyboard(mario);
    input.listenTo(window);

    const timer = new Timer(1/60);
    timer.update = function update(deltaTime) {
        level.update(deltaTime);

        camera.pos.x = Math.max(0, mario.pos.x - 100);

        level.comp.draw(context, camera);
    }

    timer.start();
}

const canvas = document.getElementById('screen');
main(canvas);
