<?php
require(APPPATH.'libraries/REST_Controller.php');

class Api extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('General_model');
    }

    public function table_get($table,$id = null){
      $respuesta = $this->General_model->read($table,$id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function table_post($table){
        
        $respuesta = $this->General_model->create($table,$this->post('data'));
        if($respuesta){
            $this->response($respuesta, 201);
        }
        else {
            $this->response('Error al crear', 401);
        }
    }

    public function table_delete($table, $id){
        $this->General_model->delete($table,$id);
        $this->response('Borrado exitosamente', 201);
    }

    public function table_put($table,$id){
        $respuesta = $this->General_model->update($table,$id,$this->put('data'));
       if($respuesta)  $this->response($respuesta, 201);
       else  $this->response("Erro al actualizar en ".$table,400);
    }

    public function sql_get(){
        $respuesta = $this->General_model->commit();
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }


    
    public function estados_get($pais_id){
        $respuesta = $this->General_model->estados($pais_id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function niveles_get($juego_id){
        $respuesta = $this->General_model->niveles($juego_id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function clasificacionesNiveles_get(){
        $clasificaciones = $this->General_model->read('clasificaciones', null);
        $clasificacionConNiveles = array();
        foreach ($clasificaciones as $clasificacion ) {
            $clasificacion['niveles'] = $this->General_model->niveles($clasificacion['ID']);
            $clasificacionConNiveles[] =$clasificacion;
        }
        $this->response($clasificacionConNiveles, 200);
    }

    public function juegosXclasificaciones_get(){
        $clasificaciones = $this->General_model->read('clasificaciones', null);
        $clasificacionConNiveles = array();
        foreach ($clasificaciones as $clasificacion) {
            $clasificacion['JUEGOS'] = $this->General_model->juegosClasificacion($clasificacion['ID']);
            $clasificacionConNiveles[] = $clasificacion;
        }
        $this->response($clasificacionConNiveles, 200);
    }


    public function juego_get($juego_id){
        $this->response($this->General_model->getJuego($juego_id), 200);
    }  

    //Obtiene las preguntas por nivel

    public function preguntasXnivel_get($nive_id){
        $this->response($this->General_model->getPreguntasNivel($nive_id),200);
    }

    

}
