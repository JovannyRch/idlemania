<?php

	class General_model extends CI_Model {

		public $unauthorized_tables = array(
			'usuarios'
		);

		public $rules = array(
			'usuarios' => array(
				'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username,estados.nombre,usuarios.fecha_registro
					 	from usuarios inner join estados on
						estados.id = usuarios.estado_id
					'
				)
			),
			'estados' => array(
				'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
				'READ' => array(
					'type' => 'columns',
					'sentence' => 'id,nombre,pais_id'
				)
			) ,
			'jugadores' => array(
				'READ' => array(
					'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username,estados.nombre,usuarios.fecha_registro
					 	from usuarios inner join estados on
						estados.id = usuarios.estado_id where tipo = 2
					'
				)
			),
			'juegos' => array(
				'operations' => ['CREATE','READ','UPDATE','DELETE' ],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						juegos.id,usuarios.username creador,juegos.IMG,juegos.nombre,juegos.tipo, juegos.clasificacion_id
					 	from usuarios inner join juegos on
						juegos.creador_id = usuarios.id 
					'
				)//Cargar la clasificacion
			)

		);



		public function __construct(){
			parent::__construct();
			$this->load->database();
			
		}

		

		//CRUD 

		public function read($table,$id = null){
			if(array_key_exists ( $table , $this->rules )){

				if(!in_array('READ',$this->rules[$table]['operations'])){
					return array();
				}
				$sql = "";
				if($this->rules[$table]['READ']['type'] == "sql"){
					$sql = $this->rules[$table]['READ']['sentence'];
				}

				if($this->rules[$table]['READ']['type'] == "columns"){
					$columns = $this->rules[$table]['READ']['sentence'];
					$sql ="SELECT $columns from $table ";
				}

				if(is_null($id)){
					return $this->db->query($sql)->result_array();
				}
				else{
					$sql= $sql."where $table.id = $id";
					return $this->db->query($sql)->row_array();
				}
			}else{
				if(is_null($id)){
					return $this->db->query("SELECT * from $table")->result_array();
				}
				else{
					$sql= "SELECT * from $table where $table.id = $id";
					return $this->db->query($sql)->row_array();
				}
			}
			
		}

		public function lastId($table){
			return $this->db->query("SELECT id from $table order by 1 desc")->row_array()['ID'];
		}
		
		public function create($table,$data){
			// Solo en ORACLE, en los demas gestores basta con $this->db->set($data)->insert($table);
			$sql = str_replace('"', '', $this->db->set($data)->get_compiled_insert($table));
			$id = $this->db->query($sql);
			if ($this->db->affected_rows() === 1) {
				return $this->read($table, $this->lastId($table));
			}else{
				return null;
			}
		}

		public function delete($table,$id){
			$query = "DELETE FROM $table where id = $id";
			$this->db->query($query);
		}

		public function update($table, $id,$data){	
			//  Para los demas gestores que no sean ORACLE 
			// $this->db->set($data)->where('id', $id)->update($table);
			$sql = str_replace('"', '', $this->db->set($data)->where('id', $id)->get_compiled_update($table));
			$this->db->query($sql);
			if ($this->db->affected_rows() === 1){
				return $this->read($table, $id);
			}else return null;
		}

		public function commit(){
			/*	$this->db->query("INSERT INTO continentes(nombre) values('América')");
			$this->db->query("INSERT INTO continentes(nombre) values('Europa')");
			$this->db->query("INSERT INTO continentes(nombre) values('África')");
			$this->db->query("INSERT INTO continentes(nombre) values('Asia')");
			$this->db->query("INSERT INTO continentes(nombre) values('Ocenanía')");
			$this->db->query("INSERT INTO continentes(nombre) values('Antártida')");
*/
			return ':)';
		}

		//Guards
		public function isNotEnable($table){
			return in_array($table, $this->unauthorized_tables);
		}

		public function isOperationEnable($table, $operation){
			//return in_array($operation, $thi->rules
		}


		//Servicios

		public function estados($pais_id){
			return $this->db->query("SELECT id,nombre from estados where pais_id = $pais_id")->result_array();
		}

		//Obtiene todas las preguntas de un nivel
		public function preguntas($nivel){
			return $this->db->query("SELECT id,cuerpo,img,repuestas from preguntas where nivel_id = $nivel")->result_array();
		}

		//Obtiene los niveles de un juego con sus respectivas respuetas
		public function niveles($juego_id){
			$niveles = $this->db->query("SELECT id,nombre from niveles where juego_id = $juego_id")->result_array();
			$resultado = array();
			foreach ($niveles as $nivel) {
				
				$nivel['preguntas'] = $this->preguntas($nivel['ID']);
				$resultado[] = $nivel;
			}
			return $resultado;

		}

		//Obtiene las clasisficaciones por nivel
		public function nivelesXclasificacion($clasificacion){
			return $this->db->query("SELECT id,img,nombre from niveles where clasificacion_id = $clasificacion")->result_array();
		}

		public function clasificaciones(){
			return $this->db->query("SELECT * from clasificaciones")->result_array();
		}

		//Obtiene todos los juegos por clasificacion
		public function juegosClasificacion($clasificacion){
			return $this->db->query("SELECT 
			juegos.id,usuarios.username creador,juegos.nombre,juegos.tipo, juegos.img
			 from usuarios inner join juegos on
			juegos.creador_id = usuarios.id  and juegos.clasificacion_id = $clasificacion")->result_array();
	
			
		}

		//Obtener los niveles de un juego
		public function getNiveles($juego_id){
			return $this->db->query("SELECT id,nombre from niveles where juego_id = $juego_id order by 1")->result_array();
		}


		public function getJuego($juego_id){
			$juego = $this->read('juegos',$juego_id);
			$clasificacion_id = $juego['CLASIFICACION_ID'];
			$juego['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
			$juego['NIVELES'] =  $this->getNiveles($juego_id);
			return $juego;
		}

		public function getPreguntasNivel($nive_id){
			return $this->db->query("SELECT id,cuerpo,img,respuestas from preguntas where nivel_id = $nive_id")->result_array();
		}
	}
?>
